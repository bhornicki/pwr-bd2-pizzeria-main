\c GalakPizza
--DROP USER (uncomment these below while recreating db)
-- REASSIGN OWNED BY restaurant TO postgres;
-- DROP OWNED BY restaurant;
-- DROP USER restaurant;
-- DROP USER IF EXISTS admin;
--DROP TABLES, VIEWS...
DROP FUNCTION IF EXISTS billadd CASCADE;
DROP FUNCTION IF EXISTS billsub CASCADE;
DROP VIEW IF EXISTS zamowienia;
DROP VIEW IF EXISTS stoliki;
DROP TABLE IF EXISTS orders CASCADE;
DROP TABLE IF EXISTS bills CASCADE;
DROP TABLE IF EXISTS order_status CASCADE;
DROP TABLE IF EXISTS employees CASCADE;
DROP TABLE IF EXISTS employee_jobs CASCADE;
DROP VIEW IF EXISTS menu;
DROP TABLE IF EXISTS product_ingredients CASCADE;
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE IF EXISTS ingredients CASCADE;
DROP TABLE IF EXISTS product_types CASCADE;
DROP TABLE IF EXISTS product_units CASCADE;

DROP SEQUENCE IF EXISTS BILL_SEQ;
DROP SEQUENCE IF EXISTS ORDER_SEQ;
DROP SEQUENCE IF EXISTS EMPLOYEE_SEQ;
DROP SEQUENCE IF EXISTS PRODUCT_SEQ;
DROP SEQUENCE IF EXISTS INGREDIENT_SEQ;

-------------------------------------------------
-- SEQUENCES
-------------------------------------------------
CREATE SEQUENCE IF NOT EXISTS BILL_SEQ INCREMENT 1 MINVALUE 1 MAXVALUE 999999999 START 1 NO CYCLE;

CREATE SEQUENCE IF NOT EXISTS ORDER_SEQ INCREMENT 1 MINVALUE 1 MAXVALUE 999999999 START 1 NO CYCLE;

CREATE SEQUENCE IF NOT EXISTS EMPLOYEE_SEQ INCREMENT 1 MINVALUE 1 MAXVALUE 999 START 1 NO CYCLE;

CREATE SEQUENCE IF NOT EXISTS PRODUCT_SEQ INCREMENT 1 MINVALUE 1 MAXVALUE 999 START 1 NO CYCLE;

CREATE SEQUENCE IF NOT EXISTS INGREDIENT_SEQ INCREMENT 1 MINVALUE 1 MAXVALUE 999 START 1 NO CYCLE;



-------------------------------------------------
-- PRODUCTS/INGREDIENTS
-------------------------------------------------
CREATE TABLE product_types
(
	id		SERIAL4		PRIMARY KEY,
	name	VARCHAR(20)	NOT NULL
);

CREATE TABLE product_units
(
	id		SERIAL4		PRIMARY KEY,
	name	VARCHAR(20)	NOT NULL
);

CREATE TABLE ingredients(
	id		INT4	PRIMARY KEY DEFAULT nextval('ingredient_seq'),
	name 	varchar(20) NOT NULL
);


CREATE TABLE products(
	id			INT4			PRIMARY KEY DEFAULT nextval('product_seq'),
	name 		varchar(40)		NOT NULL,
	quantity	int4,
	buy_price	numeric(6,2)	NOT NULL,
	type 		int4			REFERENCES product_types(ID) NOT NULL,
	size		int4 			NOT NULL,
	unit		int4			REFERENCES product_units(ID) NOT NULL
);

CREATE TABLE product_ingredients(
	product_id 		int4	REFERENCES products(ID) NOT NULL,
	ingredients_id	int4	REFERENCES ingredients(ID) NOT NULL
);

-------------------------------------------------
-- EMPLOYEES
-------------------------------------------------
CREATE TABLE employee_jobs
(
	id		SERIAL4		PRIMARY KEY,
	name	VARCHAR(20)	NOT NULL
);


CREATE TABLE employees
(
	id			INT4			PRIMARY KEY DEFAULT nextval('employee_seq'),
	firstName   varchar(20) 	NOT NULL,
	lastName	varchar(40)		NOT NULL,
	jobTitle	int4	 		REFERENCES employee_jobs(ID) NOT NULL,
	password	int4			NOT NULL
);


-------------------------------------------------
-- ORDERS/BILLS
-------------------------------------------------
CREATE TABLE order_status
(
	id		SERIAL4		PRIMARY KEY,
	name	VARCHAR(32)	NOT NULL
);

CREATE TABLE bills
(
	id			INT4			PRIMARY KEY DEFAULT nextval('bill_seq'),
	clientName	varchar(70) 	NOT NULL,
	billvalue	numeric(6,2)	DEFAULT 0 NULL,
	tableNr		int4,
	waiterID	int4	 		REFERENCES employees(ID) NOT NULL
);


CREATE TABLE orders
(
	id			INT4			PRIMARY KEY DEFAULT nextval('order_seq'),
	billID	    int4			REFERENCES bills(ID) NOT NULL,
	orderDate	date			NOT NULL,
	status		int4			REFERENCES order_status(ID) NOT NULL,
	comments	varchar(255),
	productID 	int4			REFERENCES products(ID) NOT NULL,
	cookID		int4	 		REFERENCES employees(ID)
);

-------------------------------------------------
-- INDEX'ES
-------------------------------------------------

CREATE INDEX orders_date_index on orders(orderdate DESC);
CREATE INDEX product_index on products(id);

-------------------------------------------------
-- CONSTRAINTS
-------------------------------------------------
ALTER TABLE employee_jobs
	ADD CONSTRAINT CK_employee_jobs
	CHECK (name ~ '^[A-ZŻŹĆĄŚĘŁÓŃ][a-zzżźćńółęąś]*$');
   
ALTER TABLE product_types
	ADD CONSTRAINT CK_product_types
	CHECK (name ~ '^[A-ZŻŹĆĄŚĘŁÓŃ][a-zzżźćńółęąś ]*$');

ALTER TABLE order_status
	ADD CONSTRAINT CK_order_status
	CHECK (name ~ '^[A-ZŻŹĆĄŚĘŁÓŃ][a-zzżźćńółęąś ]*$');

ALTER TABLE employees
	ADD CONSTRAINT CK_employees_firstname
	CHECK (firstname ~ '^[A-ZŻŹĆĄŚĘŁÓŃ][a-zzżźćńółęąś]*$');
	
ALTER TABLE employees
	ADD CONSTRAINT CK_employees_lastname
	CHECK (lastname ~ '^[A-ZŻŹĆĄŚĘŁÓŃ][a-zzżźćńółęąś]*$');
	
ALTER TABLE ingredients
	ADD CONSTRAINT CK_ingredients
	CHECK (name ~ '^[a-zzżźćńółęąś\- ]+$');

ALTER TABLE employees
	ADD CONSTRAINT CK_employees_password
	CHECK ("password" >= 0 );

ALTER TABLE products
	ADD CONSTRAINT CK_products_size
	CHECK (size > 0 );
	
ALTER TABLE products
	ADD CONSTRAINT CK_products_quantity
	CHECK (quantity >= 0 );
	
ALTER TABLE products
	ADD CONSTRAINT CK_products_name
	CHECK (name ~ '^[A-ZŻŹĆĄŚĘŁÓŃa-zzżźćńółęąś ]*$');

ALTER TABLE bills
	ADD CONSTRAINT CK_bills_name
	CHECK (clientname ~ '^[A-ZŻŹĆĄŚĘŁÓŃa-zzżźćńółęąś ]*$');


-------------------------------------------------
-- TRIGGERS
-------------------------------------------------


CREATE OR REPLACE FUNCTION billadd()
	RETURNS trigger AS
$$
BEGIN
    	UPDATE bills set billvalue = billvalue + (SELECT P.buy_price FROM products P WHERE P.id = NEW.productid)
        WHERE id = NEW.billid;
        
    RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER billadd_trigger
AFTER INSERT ON orders
FOR EACH ROW
EXECUTE PROCEDURE billadd();



CREATE OR REPLACE FUNCTION billsub()
	RETURNS trigger AS
$$
BEGIN
    	UPDATE bills set billvalue = billvalue - (SELECT P.buy_price FROM products P WHERE P.id = OLD.productid)
        WHERE id = OLD.billid;
        
    RETURN OLD;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER billsub_trigger
AFTER DELETE ON orders
FOR EACH ROW
EXECUTE PROCEDURE billsub();

-------------------------------------------------
-- EXAMPLE VALUES: DICTIONARIES
-------------------------------------------------
INSERT INTO product_types (NAME) VALUES
	('Pizza'),
	('Napój'),
	('Sałatka'),
	('Sos');
	 
INSERT INTO product_units (NAME) VALUES 
	('g'),
	('ml'),
	('cm'),
	('Kieliszek mały');	--https://pl.wikipedia.org/wiki/Kategoria:Dawne_jednostki_miar_i_wag

INSERT INTO employee_jobs (NAME) VALUES 
	('Kierownik'),
	('Kelner'),
	('Kucharz');

INSERT INTO order_status (NAME) VALUES 
	('Zamówione'),
	('W trakcie przygotowywania'),
	('Do dostarczenia'),
	('Dostarczone');


-------------------------------------------------
-- EXAMPLE VALUES: INGREDIENTS & PRODUCTS	TODO
-------------------------------------------------
INSERT INTO ingredients (NAME) VALUES
	('ser'),
	('ananas'),
	('sos pomidorowy'),
	('salami'),
	('szynka'),
	('oregano'),
	('rukola'),
	('szynka'),
	('cebula');

INSERT INTO products (type, name, size, unit, buy_price) VALUES
	(1,'Hawajska',30,3, 15.99),
	(1,'Hawajska',50,3, 25.99),
	(1,'Margherita',30,3, 9.99),
	(1,'Margherita',50,3, 12.99),
	(4,'Keczup',25,1, 2.00),
	(3,'Sałatka jakaśtam',150,1, 8.99);
	
INSERT INTO products (type, name, size, unit, buy_price, quantity) VALUES
	(2,'Herbata',250,2, 5.00, 199),
	(2,'Coca Cola',500,2, 4.00, 64);

INSERT INTo product_ingredients (product_id, ingredients_id) VALUES
	(1,2),	--hawajska 30
	(1,1),
	(1,6),
	(2,2),	--hawajska 50
	(2,1),
	(2,6),
	(3,3),	--margh 30
	(3,6),
	(4,3),	--margh 50
	(4,6),
	(5,3),	--keczup
	(6,7),	--sałatka
	(6,2);

-------------------------------------------------
-- EXAMPLE VALUES: EMPLOYEES
-------------------------------------------------
INSERT INTO employees (jobtitle,firstname, lastname, "password") VALUES
	(1, 'Domino', 'Jachaś', 123456), --maybe TODO: jobtitle from text?
	(2,'Jan','Nowak',111111),
	(3,'Antoni','Giecośtam',9999),
	(2,'Abc','Def',12345);

-------------------------------------------------
-- EXAMPLE VALUES: BILLS & ORDERS
-------------------------------------------------
INSERT INTO bills (waiterid, tablenr, clientname)	VALUES
	(2,5,'Alicja'),
	(2,6,'Bob'),
	(2,1,'Cezary');

INSERT into orders (orderdate, billid, productid, status ) VALUES
	(current_timestamp, 1, 7, 4),	--herbata
	(current_timestamp, 3, 8, 1);	--coca cola
	
INSERT into orders (orderdate,billid, productid, "comments", status, cookid ) VALUES
	(current_timestamp, 2, 1, 'bez sera', 3, 3);	--hawajska 30

INSERT into orders (orderdate,billid, productid, status, cookid ) VALUES
	(current_timestamp, 3, 4, 2, 3);	--margherita 50
INSERT into orders (orderdate,billid, productid, status) VALUES
	(current_timestamp, 1, 3, 1);		--margherita 30



-------------------------------------------------
-- INVALID VALUE TESTS
-------------------------------------------------
-- INSERT INTO employee_jobs (NAME) VALUES ('test');	--starts with lowercase
-- INSERT INTO employee_jobs (NAME) VALUES ('Abe3de');	--contains number
-- INSERT INTO employees (jobtitle,firstname, lastname, "password") VALUES (4,'Andrzej','Niepoprawny',123);	--invalid index	
-- INSERT INTO employees (jobtitle,firstname, lastname, "password") VALUES (3,'Andżej','Niepoprawny',-1);	--invalid password
-- INSERT INTO products (type, name, size, unit, buy_price, quantity) VALUES (2,'Te5t',1,1,1,1);	--contains number
-- INSERT INTO products (type, name, size, unit, buy_price, quantity) VALUES (2,'Test',0,1,1,1);	--zero or negative size
-- INSERT INTO products (type, name, size, unit, buy_price, quantity) VALUES (2,'Tesst',1,1,1.234,-1);	--negative quantity

-------------------------------------------------
-- VIEWS
-------------------------------------------------


CREATE VIEW MENU
("id","product_name","product_type","size","ingredients","price") AS
SELECT
	P.id,
	P.name,
	(SELECT name FROM product_types WHERE id = P.type),
	(P.size  || ' ' ||  (SELECT name FROM product_units WHERE id=P.unit)),

(SELECT STRING_AGG (I.name, ', ') FROM product_ingredients G JOIN ingredients I ON G.ingredients_id = I.id WHERE G.product_id = P.id),

P.buy_price
FROM
	products P;
    
CREATE VIEW ZAMOWIENIA
("Zamowienie","Status","Skladniki","Komentarz","Kucharz") AS
SELECT
	(SELECT P.name  || ' ' ||  P.size   || ' ' ||  U.name FROM products P JOIN product_units U ON P.unit = U.id WHERE P.id = O.productid),
	(SELECT S.name from order_status S WHERE S.id = O.status),

	(SELECT STRING_AGG (I.name, ', ') FROM product_ingredients G JOIN ingredients I ON G.ingredients_id = I.id WHERE G.product_id = O.productid),

	O.comments,
	O.cookID
FROM 
	orders O;
 

CREATE VIEW STOLIKI
("Stolik","Kelner","Kwota_do_zaplaty") AS
SELECT
	B.tableNr,
	(SELECT E.firstname   || ' ' ||  E.lastname FROM employees E where E.id = B.waiterID),
	B.billvalue
FROM
	bills B;
    
-------------------------------------------------
-- USERS & SECURITY
-------------------------------------------------
CREATE USER admin WITH PASSWORD 'Qwerty1';
ALTER USER admin WITH SUPERUSER;

CREATE USER restaurant WITH PASSWORD '123456';
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE bills, orders, employees, ingredients, products, product_ingredients, product_units TO restaurant;
GRANT SELECT ON TABLE employee_jobs, order_status, product_types TO restaurant;
GRANT SELECT ON menu TO restaurant;
GRANT SELECT,UPDATE, INSERT, DELETE ON menu, stoliki, zamowienia TO restaurant;
GRANT USAGE ON ALL SEQUENCES IN SCHEMA public TO restaurant;


 
